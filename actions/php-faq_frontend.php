<?
// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

require_once(MYFAQ_DIR.'lib/includes/util.php');
class PHP_faq_frontend {
	
	function __construct() {
        // noprov version of the edit form - ajax ajax for users who are not logged in
		add_action('wp_ajax_nopriv_edit_form', array(&$this, 'edit_form'));
	}
	
    // outputs the faq for frontend display
	public function show_faq() {
        ob_start();
        //Register stylesheet
        wp_register_style( 'php-faq-style', plugins_url('css/style.css', dirname(__FILE__)));
        wp_enqueue_style( 'php-faq-style' );
        // register the script
		wp_register_script('frontend_script', plugins_url('js/frontend.js', dirname(__FILE__)));
        // enqueue the script (causes it to be echoed at some point during output of the <head>)
		wp_enqueue_script('frontend_script', false, array('editor'));
		$model = Model::getInstance();
        $TPL->faq = $model->getFAQ();
        $faq_style = get_option('faq_frontend_style');
        include MYFAQ_DIR."templates/frontend.list.php";
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}
    // outputs the question form for frontend display
	public function show_form() {
        ob_start();
		wp_register_script('frontend_question_form_script', plugins_url('js/frontend_question_form.js', dirname(__FILE__)));
		wp_enqueue_script('frontend_question_form_script', false, array('editor'));
		include(MYFAQ_DIR.'templates/question_form_with_header.php');
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}
	
	// ajax form submit handler
	public function edit_form() {
		// frontend form currently won't work if you're logged in as admin, however admins should use the backend form anyways.
        
        // validate form input
		$result->errors = Util::test_form(true);
		if (!$result->errors) {
			$data = Util::get_form_values();
			$model = Model::getInstance();
			$result->success = $model->form_input($data);
			if ($result->success) {
				$result->errors['form_input_result'] = true; // not really an error but it makes the loop that adds labels look nice...
			}
		}
        // echo json validation info
		echo json_encode($result);
		die();
	}
}
?>
