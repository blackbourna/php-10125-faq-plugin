<?
// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.
 
// backend controller
require_once(MYFAQ_DIR.'lib/includes/util.php');
// see http://codex.wordpress.org/Function_Reference/wp_get_current_user
// used to initlize the globally used $current_user variable
class PHP_faq_backend {
	// contructor adds backend actions
	function __construct() {
		// custom ajax actions, prefix with wp_ajax_
		// NOTE: use wp_ajax_nopriv_ naming convention for frontend ajax calls
		add_action('wp_ajax_edit_form', array(&$this, 'edit_form'));
		add_action('wp_ajax_delete', array(&$this, 'delete'));
		add_action('wp_ajax_reorder', array(&$this, 'reorder'));
		add_action('wp_ajax_list_question_form', array(&$this, 'list_question_form'));
		add_action('wp_ajax_frontend_style_form_submit', array(&$this, 'frontend_style_form_submit'));
		
		// add menu "admin_menu" action to initialize admin menus
		add_action('admin_menu', array(&$this, 'build_admin_menu'));
	}
	
	// creates the backend admin menu items
	public function build_admin_menu() {
		$admin_file = MYFAQ_DIR . 'actions/php-faq_backend.php';
		add_menu_page(
			"PHP 10125 Faq Manager", // page title
			"PHP Faq", // menu title
			8, // capability
			$admin_file, // menu slug ???
			array(&$this, 'show_backend_panel'), // function call
			plugins_url('images/faq_icon.gif', dirname(__FILE__)) // icon URL
		);
		add_submenu_page(
			$admin_file, // parent slug
			"PHP 10125 Faq Manager - Frontend Style", // page title
			"Frontend Style", // menu title
			8, // capability
			'frontend-style', // menu slug ???
			array(&$this	, 'frontend_style') // function call
		);
	}
	// displays the main backend menu
    public function show_backend_panel() {
        //Register stylesheet
        wp_register_style( 'php-faq-style', plugins_url('css/style.css', dirname(__FILE__)));
        wp_enqueue_style( 'php-faq-style' );
        
		// queue JS
		wp_register_script('backend_panel_script', plugins_url('js/backend.js', dirname(__FILE__)));
		wp_enqueue_script('backend_panel_script', false, array('editor'));
		
		// wp_tiny_mce is deprecated, but the new wp_editor function is buggy when multiple instances appear on one page
		// wp_tiny_mce is also buggy when moved around the DOM but has some events that we can hook into to fix this issue (unlike wp_editor)
		// calling wp_tiny_mce before all scripts have been enqueued will cause the tinymce JS to be loaded into head
		wp_tiny_mce();
		include MYFAQ_DIR.'templates/question_form_with_header.php';
		
		$model = Model::getInstance();
        $TPL->faq = $model->getFAQ();
		
		include MYFAQ_DIR.'templates/backend.list.php';
    }
    
    // returns a single instance of the question form when list is being output, can also be called via ajax
    public function list_question_form($recno) {
        wp_enqueue_style( 'php-faq-style' );
		if (!$recno) $recno = $_REQUEST['recno'];
		$model = Model::getInstance();
        if ($_REQUEST['add_question']) { // new question added via backend
            $recno = $model->getNewestQuestionId();
            $value = $model->getQuestionById($recno);
            $ct = $model->getMaxSortOrderId() - 1;
            include(MYFAQ_DIR.'templates/backend.question_li.php');
        } else { // returns a single form, used for the backend list item show/hide
            $value = $model->getQuestionById($recno);
            include(MYFAQ_DIR.'templates/question_form.php');
        }
		die();
	}
	
	// ajax form submit handler
	public function edit_form() {
        // validate form input
		$result->errors = Util::test_form();
		if (!$result->errors) {
			$data = Util::get_form_values();
			$model = Model::getInstance();
			$result->success = $model->form_input($data);
			if ($result->success) {
				$result->errors['form_input_result'] = true; // not really an error but it makes the loop that adds labels on the client side look nice...
			}
		}
		echo json_encode($result);
		die();
	}
	
	// ajax delete handler
	public function delete() {
		$model = Model::getInstance();
		echo json_encode($model->delete($_REQUEST['recno']) === 1);
		die();
	}
	
	// ajax reorder handler
	public function reorder() {
		$model = Model::getInstance();
		parse_str($_REQUEST['list'], $list);
		echo $model->reorder($list['q']);
		die();
	}
	
	// backend menu used to set CSS styles for frontend display
	public function frontend_style() {
		// apparently WP doesn't install a jquery-ui theme by default so it's included with my plugin
		wp_register_style( 'php-faq-jquery-ui', plugins_url('js/jquery-ui/jquery-ui.css', dirname(__FILE__)));
		wp_enqueue_style( 'php-faq-jquery-ui' );
		
        // my backend styles
		wp_register_style( 'php-faq-frontend', plugins_url('css/backend_style_form.css', dirname(__FILE__)));
		wp_enqueue_style( 'php-faq-frontend' );
		
		//http://www.ilovecolors.com.ar/how-to-add-a-color-picker-to-your-wordpress-plugin-or-theme/
		wp_enqueue_style('farbtastic');
		wp_enqueue_script('farbtastic');
		wp_enqueue_script('jquery-ui-slider');
		// styles stored as WP options
        $faq_style = get_option('faq_frontend_style');
		wp_register_script('backend_style_script',  plugins_url('js/backend_style_form.js', dirname(__FILE__)));
        // localize script causes the $faq_style php variable to be available for use to the javascript in the backend_style_script's scope - NEAT
        wp_localize_script( 'backend_style_script', 'backendParams', $faq_style);
		wp_enqueue_script('backend_style_script', false, array('editor'));
		include MYFAQ_DIR."templates/backend_faq_style_form.php";
	}
	
	public function frontend_style_form_submit() {
		// if the option is updated to the same value, update_option returns false, so add a timestamp to the option to indicate that something has changed
 		if (update_option('faq_frontend_style', $_REQUEST)) {
 			die(date('r'));
 		} else {
			die('error');
		}
		
	}
}
?>
