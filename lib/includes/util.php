<?
// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

require_once(MYFAQ_DIR.'lib/model/model.php');
class Util {
    // calls trim and validate functions and returns the form valeus/validation info
	public static function test_form($frontend = false) {
		$data = Util::get_form_values();
		return Util::trim_and_validate($data, $frontend);
	}
    
    // strip special characters
	private static function clean_input($string) {
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    }
	public static function get_form_values() {
		$data = array(
			'faq_id'       => self::clean_input($_REQUEST['recno']),
			'faq_name_q'   => self::clean_input($_REQUEST['faq_name_q']),
			'faq_name_a'   => self::clean_input($_REQUEST['faq_name_a']),
			'faq_question' => self::clean_input($_REQUEST['faq_question']),
			'faq_answer'   => $_REQUEST['faq_answer'], //allow html for answers but not questions
			'published'    => self::clean_input(isset($_REQUEST['published']))
		);
		// see http://stackoverflow.com/questions/7341942/wpdb-update-or-wpdb-insert-results-in-slashes-being-added-in-front-of-quotes
		foreach ($data as $k => $v) {
			$data[$k] = stripslashes_deep($data[$k]);
		}
		return $data;
	}

	public static function trim_and_validate(&$data, $frontend = false) {
        $dontcheck = array('faq_answer', 'published', ($frontend) ? 'faq_name_a' : '');
		foreach($data as $k => $v) {
			if (in_array($k, $dontcheck)) continue;
			$data[$k] = trim($v);
			if (!$data[$k] && $k != 'faq_id') {
				$error[$k] = true;
			}
			if ($data['published'] && !$data['faq_answer']) {
				$error['faq_published'] = true;
			}
		}
		return $error;
	}
	
    // convert array to object (can cast array to obj, but not other way around)
	public static function array_to_object($array) {
		$object = new stdClass();

		foreach ($array as $key => $value) {
			$object->$key = $value;
		}
		return $object;
	}
}
?>
