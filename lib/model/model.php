<?
// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

	/*
	 *
	 * Useful wpdb class variables:
	 * 
	 * $show_errors 
	 * Whether or not Error echoing is turned on. Defaults to TRUE.
	 * $num_queries 
	 * The number of queries that have been executed.
	 * $last_query 
	 * The most recent query to have been executed.
	 * $queries 
	 * You may save all of the queries run on the database and their stop times by setting the SAVEQUERIES constant to TRUE (this constant defaults to FALSE). If SAVEQUERIES is TRUE, your queries will be stored in this variable as an array.
	 * $last_result 
	 * The most recent query results.
	 * $col_info 
	 * The column information for the most recent query results. See Getting Column Information.
	 * $insert_id 
	 * ID generated for an AUTO_INCREMENT column by the most recent INSERT query.
	 * $num_rows 
	 * The number of rows returned by the last query.
	 * $prefix 
	 * The assigned WordPress table prefix for the site.
	 * $base_prefix 
	 * The original prefix as defined in wp-config.php. For multi-site: Use if you want to get the prefix without the blog number appended. 
	 * */

class Model {
	private static $modelInstance = null;
	private $wpdb;
    // uses a singleton pattern
	public static function getInstance() {
		if (!isset(self::$modelInstance)) {
			self::$modelInstance = new Model();
		}
		return self::$modelInstance;
	}
	private function __construct() {
		global $wpdb;
		$this->wpdb = &$wpdb;
		$this->db_table = DB_FAQ;
	}
	/*
	 * Returns the number of records in the DB_FAQ table
	 * */
	public function count_records() {
		return $this->wpdb->get_var( $this->wpdb->prepare( "SELECT COUNT(*) FROM $this->db_table;" ) );;
	}
    
    /*
     * Reorders the faq items sortorder properties
     * */
	public function reorder($faqorder) {
        $ct = $this->count_records();
		for ($i = 0; $i < $ct; $i++) {
			$faq_sortorder = sprintf("%04d", $i + 1);
			
			//Update a row in the table. Returns false if errors, or the number of rows affected if successful.
			// $wpdb->update( $table, $data, $where, $format = null, $where_format = null ); 
			
			$success = $this->wpdb->update(
				DB_FAQ, 
				array( 
					'faq_sortorder' => $faq_sortorder,	// string
				), 
				array( 'faq_id' => sprintf("%04d", $faqorder[$i]) ),
				array( 
					'%s',	// $faq_sortorder
				), 
				array( '%s' ) 
			);
			if (!success) return false;
		}
        return true;
	}
	
	/*
	 * Used to return a single faq item object based on faq_id
	 * */
	public function getQuestionById($no) {
		$sql = "SELECT * from $this->db_table WHERE faq_id = %s";
		$result = $this->wpdb->get_results($this->wpdb->prepare( $sql, $no));
		return $result[0];
	}
	
	/*
	 * Used to return a single faq item object based on faq_id
	 * */
	public function getQuestionBySortOrder($no) {
		$sql = "SELECT * from $this->db_table WHERE faq_sortorder = %s";
		return $this->wpdb->get_results($this->wpdb->prepare( $sql, $no));
	}
	
    public function getNewestQuestionId() {
		$sql = "SELECT MAX(faq_id) from $this->db_table";
        return $this->wpdb->get_var($this->wpdb->prepare($sql));
    }
    
    public function getMaxSortOrderId() {
		$sql = "SELECT MAX(faq_sortorder) from $this->db_table";
        return $this->wpdb->get_var($this->wpdb->prepare($sql));
    }
    
    
	public function form_input($data) {
		if ($data['faq_id']) { // update
			$sql = 'SELECT faq_sortorder FROM '.DB_FAQ.' WHERE `faq_id` = %s';
			$sort_order = $this->wpdb->get_var($this->wpdb->prepare($sql, $data['faq_id']));
			// $wpdb->update( $table, $data, $where, $format = null, $where_format = null ); 
			$success = $this->wpdb->update(
				$this->db_table, 
				array(
					'faq_name_q'    => $data['faq_name_q'],
					'faq_name_a'    => $data['faq_name_a'],
					'faq_question'  => $data['faq_question'],
					'faq_answer'    => $data['faq_answer'],
					'published'     => $data['published'],
					'faq_sortorder' => $sort_order
				),
				array( 'faq_id' => $data['faq_id'])
			);
		} else { // insert
			$sql = "SELECT MAX( faq_sortorder ) + 1 FROM $this->db_table";
			$sort_order = $this->wpdb->get_var($this->wpdb->prepare($sql));
			// $wpdb->insert( $table, $data, $format ); 
			$success = $this->wpdb->insert(
				$this->db_table,
				array(
					'faq_name_q'    => $data['faq_name_q'],
					'faq_name_a'    => $data['faq_name_a'],
					'faq_question'  => $data['faq_question'],
					'faq_answer'    => $data['faq_answer'],
					'published'     => $data['published'],
					'faq_sortorder' => $sort_order
				)
			);
		}
		return !is_bool($success); // returns false if failure, or number of affected rows
	}
	
    public function getFAQ() {
		$sql = "SELECT 	faq_id,faq_name_q,faq_name_a,faq_question,faq_answer,faq_sortorder,published FROM $this->db_table ORDER BY faq_sortorder";
		return $this->wpdb->get_results($this->wpdb->prepare( $sql ));
    }
    
  
	public function delete($record) {
		$sql = "DELETE FROM $this->db_table WHERE faq_id = %s";
		// returns number of rows affected, or false if error
		// should always return 1 here as we only ever delete on faq item at a time
		return $this->wpdb->query($this->wpdb->prepare($sql, $record));
    }
	
	public function activate() {
		if($this->wpdb->get_var("show tables like $this->db_table;") != DB_FAQ) {
			$table_name = DB_FAQ;
			// in this case, faq name is the name of the asked
			$sql = <<< SQL
CREATE TABLE IF NOT EXISTS `$this->db_table` (
	`faq_id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
	`faq_name_q` varchar(50) DEFAULT NULL,
	`faq_name_a` varchar(50) DEFAULT NULL,
	`faq_question` text,
	`faq_answer` text,
	`faq_sortorder` int(4) unsigned zerofill DEFAULT NULL,
	`published` BOOL NOT NULL DEFAULT FALSE,
	PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47;
SQL;
			$this->wpdb->query($sql);
			$this->wpdb->insert(
				$this->db_table,
				array(
					'faq_name_q'    => 'Someone with a question',
					'faq_name_a'    => 'Someone with an answer',
					'faq_question'  => 'What is this?',
					'faq_answer'    => 'An example faq question.',
					'published'     => true
				)
			);
		}
	}
}

