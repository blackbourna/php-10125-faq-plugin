<?php
/*
Plugin Name: PHP 10125 Faq Plugin
Plugin URI: http://www.mohawkcollege.ca
Description: A Faq Manager
Version: 1.0
Author: Andrew Blackbourn
Author URI: http://www.mohawkcollege.ca
License: GPL2
*/

// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

/*
 * Define constants
 * */
global $wpdb;
define("DB_FAQ", $wpdb->prefix."php_faq");
define("MYFAQ_DIR", dirname(__FILE__) . '/');
define("WP_DEBUG", true);

// Intialize the plugin class by calling initialize on our class
add_action('init', array('PHP_faq','init'));

//register jquery-ui-sortable (rather than entire jquery-ui lib)
wp_enqueue_script('jquery-ui-sortable');

//Installation Hook, fires when Activate button is clicked
register_activation_hook(__FILE__, array('PHP_faq', "activate"));

// initialize backend if admin
if (is_admin()) {
	require_once(MYFAQ_DIR.'actions/php-faq_backend.php');
	new PHP_faq_backend();
}
// initialize frontend
require_once(MYFAQ_DIR.'actions/php-faq_frontend.php');
new PHP_faq_frontend();

//add frontend post/page shortcodes
require_once(MYFAQ_DIR.'actions/php-faq_frontend.php');
add_shortcode('php_faq', array('PHP_faq_frontend', 'show_faq'));
add_shortcode('php_faq_form', array('PHP_faq_frontend', 'show_form'));

class PHP_faq {
	//
	public function init() {
		//require_once(MYFAQ_DIR.'actions/php-faq_frontend.php');
	}
	public function activate () {
		//require_once(MYFAQ_DIR.'lib/model/model.php');
		// set up database upon installation/activation
		$model = Model::getInstance();
		$model->activate();
	}
}
?>
