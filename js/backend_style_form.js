// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

// File used for defining JS for the Frontend styling menus displayed in the Admin section
/*
Note: backendParams is a php array that has been injected into this javascript is localized via wp_localize_script in PHP_faq_backend::frontend_style
*/
jQuery(function(){
	// don't use $ globally in WP
	var $ = jQuery;
	var pathname = window.location.pathname;
	pathname = pathname.substring(0, pathname.lastIndexOf('/'));
	/* Generic function to create a horizontal JQ-ui slider with default values
     * 
     *  @param id = the element id
	 * @param slideFunc(event, ui) = the function to run when then slider changes values
     * @param val - the default value of the slider
	 * @param mn - the slider minimum (optional)
	 * @param mx - the slider maximum (optional)
	 * */
	var makeSlider = function(id, slideFunc, val, mn, mx) {
		$(id+"_slider").slider({
			orientation: "horizontal",
			range: "min",
			min: ((mn) ? mn : 0),
			max: ((mx) ? mx : 60),
			value: val ? val : mn,
			slide: slideFunc
		});
		$(id).val($(id+"_slider" ).slider("value"));
	}
	// padding slider
	makeSlider("#faq_padding",
		function( event, ui ) {
			$( "#faq_padding" ).val( ui.value );
			$('.faq_li').css('padding', ui.value+"px");
			$('.faq_li li:not(li.info)').css('padding-top', ui.value+"px");
		},
        backendParams.faq_padding
	);
	// font size slider
	makeSlider("#faq_font_size",
		function( event, ui ) {
			$( "#faq_font_size" ).val( ui.value );
			$( "#faq_line_height" ).val( ui.value );
			$('#li_faq_question').css('font-size', ui.value);
			$('.faq_li li').css('font-size', ui.value*0.75);
			$('.faq_li li.info').css('font-size', ui.value*0.5);
			$('.faq_li li.info em').css('font-size', ui.value*0.5);
			$('#q').children().css('line-height', ui.value/15);
		},
        backendParams.faq_font_size,
		8, 36
	);
	// border radius slider
	makeSlider("#faq_border_radius",
		function( event, ui ) {
			$( "#faq_border_radius" ).val( ui.value );
			$('.php_faq.question_list li').css('border-radius', ui.value+"px");
		},
        backendParams.faq_border_radius
	);
	
	// border size slider
	makeSlider("#faq_border",
		function( event, ui ) {
			$( "#faq_border" ).val( ui.value );
			$('.php_faq.question_list li.faq_li').css('border', ui.value+"px solid");
		},
        backendParams.faq_border
	);
	
	// box shadow slider
	makeSlider("#faq_box_shadow",
		function( event, ui ) {
			$( "#faq_box_shadow" ).val( ui.value );
			$('.php_faq.question_list li.faq_li').css('box-shadow', ui.value+"px " + (ui.value > 0 ? ui.value : -ui.value)+"px " + (ui.value > 0 ? ui.value : -ui.value)+"px " + " #888");
		},
        backendParams.faq_box_shadow,
        -15,15
	);
	
	// need to figure out how to get this to intelligent default
	$('#q').children().css('padding', $('#faq_padding').val());

	// background color
	var background_color = $.farbtastic('#colorpicker_background_color', function(color) {
		$('#background_color').val(color);
		$(".php_faq.question_list li").css("background-color", color);
	});
    // set the colorpicker color
    if (backendParams.background_color) {
        background_color.setColor(backendParams.background_color);
    }
    
	// font color
	var font_color = $.farbtastic('#colorpicker_font_color', function(color) {
		$('#font_color').val(color);
		$(".php_faq.question_list").children().css("color", color);
	});
    // set the colorpicker color
    if (backendParams.font_color) {
        font_color.setColor(backendParams.font_color);
    }
    
	// font
	$('#frontend_style select').change(function() {
		$('.php_faq.question_list *').css('font-family', $(this).val());
	});
	$('*<.php_faq.question_list').css('font-family', $("#frontend_style select").val());
	$('form#frontend_style').submit(function(event) {
		var form = $(this);
		var formData = form.serialize();
		var action = form.attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: formData,
			success: function(data) {
				if (data != 'error') {
					$('form#frontend_style .form_input_result').css('background-color', '#6F6').html('Successfully updated.');
					// update the hidden timestamp field (see backend::frontend_style_form_submit)
                    // if the form is submitted with the same values as what's already in the DB, it returns a failure otherwise
					$('form#frontend_style').find('#timestamp').val(data);
				} else {
					$('form#frontend_style .form_input_result').css('background-color', '#F66').html('There was a problem updating the frontend styles.');
				}
				$('form#frontend_style .form_input_result').show().hide(5000);
			}
		});
		event.preventDefault();
	});
    // hide form
	$('#faq_custom_style').click(function() {
		$('form#frontend_style table').toggle();
	});
    // show form when "Use Custom Style" checkbox is selected
    if (backendParams.faq_custom_style == 0) {
        $('#faq_custom_style').prop('checked', true);
        $('form#frontend_style table').show();
    }
    //console.log(backendParams);
    $("ul.php_faq.question_list li").click(function() { $(this).find("ul").toggle(500); });
});
