// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

jQuery(function(){
	// don't use $ globally in WP
	var $ = jQuery;
	var pathname = window.location.pathname;
	pathname = pathname.substring(0, pathname.lastIndexOf('/'));
	var ajaxUrl = pathname+'/admin-ajax.php'; // all ajax calls through wordpress go through admin-ajax.php - even from the frontend!!!
	
	// hide error/success messages in form when first loaded, loads tinymce editor into answer field
	var initializeForm = function(formDiv) {
		formDiv.find('.success, .error').hide();
		// hide answer and question answerer name field
		var hiddenFields = [
			$(formDiv.find('[for^=faq_name_a],[id^=faq_name_a]')), // admin label and field
			$(formDiv.find('[for^=faq_answer],[id^=faq_answer]')) // answer label and field
		];
		for (var v in hiddenFields) {
			hiddenFields[v].css('display', 'none');
		}
		$(formDiv.find('[id^=faq_name_a]')).val('notset');
	}
	// new question has been submitted from frontend, submit via ajax
	var newQuestionSubmitEvent = function(event) {
		var form = $(this);
		
		var formData = form.serialize();
		var recno = form.find('input[name=recno]').val();
		
		var action = form.attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: formData,
			success: function(data) {
				data = $.parseJSON(data);
				if (data.success) {
                    form.find('.success').fadeIn(1000).fadeOut(2000);
                    // jquery has no reset function for forms, must use dom
                    // http://stackoverflow.com/questions/2316199/jquery-get-dom-node
                    form.get(0).reset();
				} else {
                    // show error messages (keys of array are classnames for the labels)
					for (var className in data.errors) {
						form.find('.error.'+className).fadeIn(1000);
					}
				}
			}
		});
		event.preventDefault();
	};
	
	// new question form init
	var addQuestionForm = $('.php_faq.new_question');
    initializeForm(addQuestionForm);
    addQuestionForm.find('form').submit(newQuestionSubmitEvent);
    addQuestionForm.find('.del_link, input[type=checkbox], label[for=published_]').hide(); // hide publish and delete buttons
    addQuestionForm.delay(1000).css('visibility', 'visible');
});
