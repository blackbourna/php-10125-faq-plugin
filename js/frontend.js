// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

jQuery(function(){
	// don't use $ globally in WP
	var $ = jQuery;
    // frontend faq item show/hide
    $("ul.php_faq.question_list ul").hide();
    $("ul.php_faq.question_list li").click(function() { $(this).find("ul").toggle(500); });
});
