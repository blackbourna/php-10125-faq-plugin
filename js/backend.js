// Andrew Blackbourn 000129408
// StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else.

jQuery(function(){
	// don't use $ globally in WP
	var $ = jQuery;
    // get the pathname, as we it for ajax form submissions
	var pathname = window.location.pathname;
	pathname = pathname.substring(0, pathname.lastIndexOf('/'));
	// all ajax calls through wordpress go through admin-ajax.php - even from the frontend!!!
    var ajaxUrl = pathname+'/admin-ajax.php';

	// for using multiple dynamically added tinymce instances
	//http://blog.mirthlab.com/2008/11/13/dynamically-adding-and-removing-tinymce-instances-to-a-page/
    tinyMCE.init({
        mode : "none",
        theme : "advanced"
    });
	
	// hide error/success messages in form when first loaded, loads tinymce editor into answer field
	var initializeForm = function(formDiv) {
		formDiv.find('.success, .error').hide();
		formDiv.find('form').submit(formSubmitEvent);
        
        // add Tiny MCE to answer textarea box
		var answerTextArea = formDiv.find('[id^=faq_answer]').attr('id');
		tinyMCE.execCommand('mceAddControl', false, answerTextArea);
	}
	
	// faq item show/hide
	var toggleFaqItem = function() {
		var formDiv = $(this).parent().children('.faq_form');
		if ($(this).parent().children('.faq_form').children().length == 0) { // faq item is currently not expanded, so fetch the item
			$.ajax({
				url: ajaxUrl,
				data: {action: 'list_question_form', recno: $(this).next(['input[name=recno']).val()},
				success:function(data){
					formDiv.html(data).hide().show(1000);
					initializeForm(formDiv);
				}
			});
		} else { // close the faq item
			var answerTextArea = formDiv.find('[id^=faq_answer]').attr('id');
            // remove the MCE object, as it's events do not clean up nicely without manually removing it
			tinyMCE.execCommand('mceFocus', false, answerTextArea);
			tinyMCE.execCommand('mceRemoveControl', false, answerTextArea);
			$(this).parent().children('.faq_form').children().hide(1000, function() {$(this).parent().empty()});
		}
	}
	// show/hide init
	$('.listquestions .faq_item #li_faq_question').click(toggleFaqItem);
	
	// sortable drop event
	var sortableDropped = function(event, ui) {
        var msg = "Updating...";
        if (event == false) msg = $('#result').text() + " " + msg;
		$('#result').text(msg).show();
		$.ajax({
			url: ajaxUrl,
			data: { list: $('.listquestions').sortable('serialize'), action: 'reorder'},
			success:function(data) {
				// event is only false when a delete event calls sortableDropped
				data = $.parseJSON(data);
				if (data) {
					var msg = 'Table was successfully updated.';
					if (event == false) msg = $('#result').text() + " " + msg;
					$('#result').text(msg).hide(2000);
				} else {
					var msg = 'There was a problem updating the table during reordering. Please refresh and try again.';
					if (event == false) msg = $('#result').text() + " " + msg;
					$('#result').text(msg).css('color', 'red');
				}
			}
		});
        // renumber the faq items so we don't skip indexes
		$('.listquestions li').each(function(index, element) {
			$(element).find('#faq_id').html(index + 1 + ":");
		});
	};
	
	// sortable list
	$(".listquestions").sortable({
        // reorder automatically
        update: sortableDropped,
		items: 'li',
            start: function(event, ui) {
                var faqItem = $(ui.item);
                var answerTextArea = faqItem.find('[id^=faq_answer]').attr('id');
                if (answerTextArea) {
                    //when we drag an open faq item, remove the MCE control and close the faq item
                    tinyMCE.execCommand('mceFocus', false, answerTextArea);
                    tinyMCE.execCommand('mceRemoveControl', false, answerTextArea);
                    faqItem.find('.faq_form').children().hide(1000, function() {$(this).parent().empty()});
                }
                // http://stackoverflow.com/questions/1771627/preventing-click-event-with-jquery-drag-and-drop
                ui.item.bind("click.prevent", function(event) { event.preventDefault(); });
            },
            stop: function(event, ui) {
				$(ui.item).find('.faq_item').removeClass('delete');
                setTimeout(function(){ui.item.unbind("click.prevent");}, 30);
            },
            // The following events are used to determine whether a dragged item is being sorted or deleted
            // http://forum.jquery.com/topic/drag-item-out-of-a-sortable
            receive: function(e, ui) {
				sortableIn = 1;
				$(ui.item).find('.faq_item').removeClass('delete'); 
			},
            over: function(e, ui) {
				sortableIn = 1;
				$(ui.item).find('.faq_item').removeClass('delete'); 
			},
            out: function(e, ui) { 
				sortableIn = 0;
				$(ui.item).find('.faq_item').addClass('delete');
			},
            beforeStop: function(e, ui) { // delete if the item is being dropped outsite of the list
                if (sortableIn == 0) { 
                    var recno = $(ui.item).find('input[name=recno]').val();
                    deleteItem(recno, ui.item);
                    event.preventDefault();
                }
            }

    });
	// delete item event
    // @recno - the record # to delete
    // the element that contains this faq item
	var deleteItem = function(recno, element) {
		$.ajax({
			url: ajaxUrl,
			data: {'recno': recno, 'action': 'delete'},
			success:function(data) {
				data = $.parseJSON(data);
				if (data) {
                    $('#result').text('Record deleted.').show();
                    element.remove();
                    sortableDropped(false, false);
                } else {
					$('#result').text('There was a problem updating the table. Please refresh and try again.').css('color', 'red').show();
                }
			}
		});
	}
	
	// form edit event
	var formSubmitEvent = function(event) {
		var form = $(this);
		
		// wow... http://www.tinymce.com/forum/viewtopic.php?id=10457
		// tinymce textareas don't play nice with jquery, need to get textarea value
		form.find('[name=faq_answer]').val(
			tinyMCE.get('faq_answer_'+form.find('input[name=recno]').val()).getContent()
		);
        // serialize the form
		var formData = form.serialize();
		
		var action = form.attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: formData,
			success: function(data) {
				data = $.parseJSON(data);
                // hide all error/success messages
				form.find('span.error,span.success').fadeOut(500);
				if (data.success) {
					var parentDiv = form.parent().closest('.faq_item');
                    // show success message
					form.find('.success').fadeIn(1000).fadeOut(2000);
                    // turn green if published, red otherwise
					if (form.find('input[name=published]').is(':checked')) {
						parentDiv.addClass('published');
					} else {
						parentDiv.removeClass('published');
					}
                    // update the list item label
					var label = form.parent().parent().find('#li_faq_question');
					label.html(form.find('textarea[name=faq_question]').val());
				} else {
                    // show error messages (array keys are classnames for the labels)
					for (var className in data.errors) {
						form.find('.error.'+className).fadeIn(1000);
					}
				}
			}
		});
		event.preventDefault();
	};
	
	var newQuestionSubmitEvent = function(event) {
		var form = $(this);
		
		// wow... http://www.tinymce.com/forum/viewtopic.php?id=10457
		// tinymce textareas don't play nice with jquery
		form.find('[name=faq_answer]').val(
			tinyMCE.get('faq_answer_'+form.find('input[name=recno]').val()).getContent()
		);
		var formData = form.serialize();
		var recno = form.find('input[name=rlistquestionsecno]').val();
		
		var action = form.attr('action');
		$.ajax({
			type: "POST",
			url: action,
			data: formData,
			success: function(data) {
				data = $.parseJSON(data);
				if (data.success) {
                    // show success message
                    form.find('.success').fadeIn(1000).fadeOut(2000);
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: {"action": "list_question_form", "add_question": true},
                        success: function(data) {
                            // append new list item
                            $('.listquestions').append(data);
                            $('.listquestions li').last().find('#li_faq_question').click(toggleFaqItem);
							// jquery has no reset function for forms, must use dom
							// http://stackoverflow.com/questions/2316199/jquery-get-dom-node
							form.get(0).reset();
                        }
                    });
				} else {
                    // show error messages (array keys are classnames for the labels)
					for (var className in data.errors) {
						form.find('.error.'+className).fadeIn(1000);
					}
				}
			}
		});
		event.preventDefault();
	};
	
	// new question form init
	var addQuestionForm = $('.php_faq.new_question');
    initializeForm(addQuestionForm);
    // unbind the old submit event (set by initializeForm, otherwise it will call both submit functions)
    addQuestionForm.find('form').unbind('submit');
    addQuestionForm.find('form').submit(newQuestionSubmitEvent);
    addQuestionForm.find('.del_link, input[type=checkbox], label[for=published_]').hide(); // hide publish and delete buttons
    addQuestionForm.delay(1000).css('visibility', 'visible');
});
