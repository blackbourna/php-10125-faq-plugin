<!-- Andrew Blackbourn 000129408 -->
<!-- StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else. -->
<?// template for the backend faq list ?>
<div class='php_faq_list'>
	<h3>Faq Usage:</h3>
	<h4>To add the faq to a page use the [php_faq] shortcode</h4>
	<h4>To add the question form to a page use the [php_faq_form] shortcode</h4>
	<br />
    <h2>Faq List</h2>
    <div id='result'></div>
    <h3>Drag item out of the list to delete.</h3>
    <ul class='listquestions'>
        <? foreach ($TPL->faq as $value): ?>
            <? include(MYFAQ_DIR.'templates/backend.question_li.php'); ?>
        <? endforeach; ?>
    </ul>
</div>
