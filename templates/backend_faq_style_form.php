<!-- Andrew Blackbourn 000129408 -->
<!-- StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else. -->
<?
// available web safe fonts
$fonts = array('Georgia, serif',
	'"Palatino Linotype", "Book Antiqua", Palatino, serif',
	'"Times New Roman", Times, serif',
	'Arial, Helvetica, sans-serif',
	'"Arial Black", Gadget, sans-serif',
	'"Comic Sans MS", cursive, sans-serif',
	'Impact, Charcoal, sans-serif',
	'"Lucida Sans Unicode", "Lucida Grande", sans-serif',
	'Tahoma, Geneva, sans-serif',
	'"Trebuchet MS", Helvetica, sans-serif',
	'Verdana, Geneva, sans-serif',
	'"Courier New", Courier, monospace',
	'"Lucida Console", Monaco, monospace');
?>
<?php if (isset($faq_style['faq_custom_style'])): ?>
<style type="text/css">
	.php_faq.question_list * {
		font-family: <?= $faq_style['font']; ?>;
		font-size: <?= $faq_style['faq_font_size']; ?>px;
		line-height: <?= $faq_style['faq_font_size']/15; ?>;
	}
	.php_faq.question_list .faq_li {
		padding: <?= $faq_style['faq_padding']; ?>px;
		list-style-position: outside;
		list-style-type: none;
		background-color: <?= $faq_style['background_color']; ?>;
		color: <?= $faq_style['font_color']; ?>;
		border-radius: <?= $faq_style['faq_border_radius']; ?>px;
		margin: 10px;
		border: <?= $faq_style['faq_border']; ?>px solid;
		<? $bdr = $faq_style['faq_box_shadow']; 
			$bdr = "{$bdr}px ".($bdr > 0 ? $bdr : -$bdr).'px '.($bdr > 0 ? $bdr : -$bdr).'px #888';
		?>
		box-shadow: <?= $bdr; ?>;
	}
	.faq_li ul.answer li.parent {
		padding: 0px;
	}
	.faq_li ul.answer li {
		padding-top: <?= $faq_style['faq_padding']; ?>px;
		font-size: <?= $faq_style['faq_font_size']*0.75; ?>px;
	}
	.php_faq.question_list .faq_li ul.answer li.info, .php_faq.question_list .faq_li ul.answer li.info em {
		padding: 0px;
		font-size: <?= $faq_style['faq_font_size']*0.5; ?>px;
	}
	.php_faq.question_list .faq_li ul {
		list-style-position: outside;
		list-style-type: none;
		margin-left: 0px;
	}
</style>
<?php endif; ?>
<h1>Frontend Style Options</h1>
<form id="frontend_style" action="<?= site_url('/wp-admin/admin-ajax.php') ?>" method="POST">
	<input type="hidden" id="action" name="action" value="frontend_style_form_submit" />
	<input type="hidden" id="timestamp" name="timestamp" value="<?= date('r'); ?>" />
	<input type="checkbox" id="faq_custom_style" name="faq_custom_style" value="<?php echo $value['faq_custom_style']; ?>" />
	<label for="faq_custom_style">Use Custom Style:</label>
	<table id="frontend_style">
		<tr>
			<td id="frontend_form">
				<!-- padding -->
				<label for="faq_padding">Padding:</label>
				<input type="text" class="noshow" id="faq_padding" name="faq_padding" value="<?php echo $value['faq_padding']; ?>" />
				<div class="slider" id="faq_padding_slider"></div>
				<br />
				<!-- font size -->
				<label for="faq_font_size">Font Size:</label>
				<input type="text" class="noshow" id="faq_font_size" name="faq_font_size" value="<?php echo $value['faq_font_size']; ?>" />
				<div class="slider" id="faq_font_size_slider"></div>
				<br />
				
				<!-- border size -->
				<label for="faq_border">Border size:</label>
				<input type="text" class="noshow" id="faq_border" name="faq_border" value="<?php echo $value['faq_border']; ?>" />
				<div class="slider" id="faq_border_slider"></div>
				<br />

				<!-- border radius -->
				<label for="faq_border_radius">Border Radius:</label>
				<input type="text" class="noshow" id="faq_border_radius" name="faq_border_radius" value="<?php echo $value['faq_border_radius']; ?>" />
				<div class="slider" id="faq_border_radius_slider"></div>
				<br />
				
				<!-- box show -->
				<label for="faq_box_shadow">Box Shadow:</label>
				<input type="text" class="noshow" id="faq_box_shadow" name="faq_box_shadow" value="<?php echo $value['faq_box_shadow']; ?>" />
				<div class="slider" id="faq_box_shadow_slider"></div>
				<br />
				
				<!-- font -->
				<label for="font">Font</label>
				<select name="font" id="font">
					<? foreach ($fonts as $font): ?>
						 <option value='<?= $font ?>' <?= ($font == $faq_style['font']) ? "selected" : ""?>><?= $font ?></option>
					<? endforeach; ?>
				</select>
				<br />
				<!-- BG Color -->
				<label for="background_color">Pick FAQ item background color</label>
				<input type="text" class="hide" id="background_color" name="background_color" value="<?php echo $value['background_color']; ?>" />
				<div id="colorpicker_background_color" class="picker" ></div>
                <div style="clear: both"></div>
                <br />
				<!-- Font Color -->
				<label for="font_color">Pick FAQ item font color</label>
				<input type="text" class="hide" id="font_color" name="font_color" value="<?php echo $value['font_color']; ?>" />
				<div id="colorpicker_font_color" class="picker"></div>
			</td>
			<td id="frontend_example">
				<ul class="php_faq question_list">
					<li id='q' class="faq_li">
						<div class='faq_item'>
							<span id='li_faq_question'>1: Is this a valid FAQ Question?</span>
						</div>
						<ul class="answer">
							<li class="info">Asked by <em>Someone</em></li>
							<li class="info">Answered by <em>Someone else</em></li>
							<li>Yes it is.</li>
						</ul>
					</li>
				</ul>
			</td>
		</tr>
	</table>
	<input type="submit" />
	<input type="reset" />
	<span class='form_input_result' ></span>
</form>
