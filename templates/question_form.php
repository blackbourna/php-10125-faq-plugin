<form id="form" method="post" action="<?= site_url('/wp-admin/admin-ajax.php') ?>" class='php_faq'>
	<?php
	/*
	 * The hidden input named "action" correlates to the wp_ajax_ action hook that we created in PHP_faq_backend::__construct
	 */
	?>
	<input type="hidden" name="action" value="edit_form" />
	<input type="hidden" name="recno" value="<?= $value->faq_id; ?>" />
	<br />
	<input type='checkbox' id='published_<?=$value->faq_id;?>' name='published' <?= ($value->published) ? 'checked' : ''; ?> /><label for='published_<?=$value->faq_id?>'>Published</label>
    <span class="error faq_published">Published with no answer</span>
	<br />
	<fieldset id="personal">
		<label for="faq_name_q_<?=$value->faq_id;?>">Asker Name: </label>
		<input type="text" name="faq_name_q" id="faq_name_q_<?=$value->faq_id;?>" value="<?= $value->faq_name_q; ?>"/>
			<span class="error faq_name_q">Asker Name field blank</span>
		<br />
		<label for="faq_name_a_<?=$value->faq_id;?>">Admin Name: </label>
		<input type="text" name="faq_name_a" id="faq_name_a_<?=$value->faq_id;?>"  value="<?
			if (strtolower($value->faq_name_a) == "notset" || strtolower($value->faq_name_a) == false) { // question is from frontend, so no admin name is given, replace that in the form with the current user's name
				get_currentuserinfo();
				global $current_user;
				echo $current_user->user_nicename;
			} else { 
				echo $value->faq_name_a; 
			}
		?>"/>
			<span class="error faq_name_a">Admin Name field blank</span>
		<br /><br />
		<label for="faq_question_<?=$value->faq_id;?>">Question : </label>
			<textarea id='faq_question_<?=$value->faq_id;?>' name="faq_question" cols="50" rows="2" wrap="virtual" id="faq_question" tabindex="2"><?= ($value->faq_question) ? ($value->faq_question) : $TPL->form->faq_question; ?></textarea>
			<span class="error faq_question">Question field blank</span>
		<br />
		<label for="faq_answer_<?=$value->faq_id;?>">Answer : </label>
		<textarea id='faq_answer_<?=$value->faq_id;?>' name="faq_answer" cols="50" rows="2" wrap="virtual" id="faq_answer" tabindex="2"><?= ($value->faq_answer) ? ($value->faq_answer) : $TPL->form->faq_answer; ?></textarea>
		<span class="error faq_answer">answer field blank</span>
		<br />
	</fieldset>
	<div>
		<input id="button1" type="submit" value="Submit this Question" />
		<input type='reset' />
	</div>
	<div>
			<span class='success form_input_result' style="width: 100%;">Successfully updated</span>
			<span class='error form_input_result' style="width: 100%;">There was an error updating this item</span>
	</div>
</form>
