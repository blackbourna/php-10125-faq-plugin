<!-- Andrew Blackbourn 000129408 -->
<!-- StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else. -->
<?//template for a single item from backend.list.php?>
<li id='q_<?= $value->faq_id; ?>'>
    <div class='faq_item <?= $value->published ? 'published' : ''; ?>'>
		<img src="../wp-content/plugins/php-faq/images/delete.gif" class="delete_icon" />
        <span id='faq_id'><?= ++$ct; ?>:</span> <span id='li_faq_question'><?= $value->faq_question; ?></span>
        <input type="hidden" name="recno" value="<?= $value->faq_id; ?>" />
        <div class = 'faq_form'>
        </div>
    </div>
</li>
