<!-- Andrew Blackbourn 000129408 -->
<!-- StAuthcomp10125: I Andrew Blackbourn, 000129408 certify that this material is my original work. No other person's work has been used without due acknowledgement. I have not made my work available to anyone else. -->
<?php if (isset($faq_style['faq_custom_style'])): ?>
<script>
    // remove any default padding that the template adds to the faq list
	jQuery('.php_faq.question_list ul:not([class])').css({"padding": "0px", "margin": "0px" });
</script>
<style type="text/css">
    /* set up css styles that were set in the backend style form */
	.php_faq.question_list * {
		font-family: <?= $faq_style['font']; ?>;
		font-size: <?= $faq_style['faq_font_size']; ?>px;
		line-height: <?= $faq_style['faq_font_size']/15; ?>;
	}
	.php_faq.question_list .faq_li {
		padding: <?= $faq_style['faq_padding']; ?>px;
		list-style-position: outside;
		list-style-type: none;
		background-color: <?= $faq_style['background_color']; ?>;
		color: <?= $faq_style['font_color']; ?>;
		border-radius: <?= $faq_style['faq_border_radius']; ?>px;
		margin: 10px;
		border: <?= $faq_style['faq_border']; ?>px solid;
		<? $bdr = $faq_style['faq_box_shadow']; 
			$bdr = "{$bdr}px ".($bdr > 0 ? $bdr : -$bdr).'px '.($bdr > 0 ? $bdr : -$bdr).'px #888';
		?>
		box-shadow: <?= $bdr; ?>;
	}
	.faq_li ul.answer li.parent {
		padding: 0px;
	}
	.php_faq.question_list .faq_li ul.answer li p {
		padding-top: <?= $faq_style['faq_padding']; ?>px;
		font-size: <?= $faq_style['faq_font_size']*0.75; ?>px;
	}
	
	.php_faq.question_list .faq_li ul.answer li.info, .php_faq.question_list .faq_li ul.answer li.info em {
		font-size: <?= $faq_style['faq_font_size']*0.5; ?>px;
	}
	
	.php_faq.question_list .faq_li ul {
		list-style-position: outside;
		list-style-type: none;
		margin-left: 0px;
	}
</style>
<?php endif; ?>
<ul class="php_faq question_list">
	<?foreach ($TPL->faq as $value):?>
		<? if (!$value->published) continue; ?>
			<?//template for a single item from frontend.list.php?>
			<li id='q_<?= $value->faq_id; ?>' class="faq_li">
				<div class='faq_item'>
					<span id='li_faq_question'><?= ++$ct; ?>: <?= $value->faq_question; ?></span>
				</div>
				<ul class="answer">
					<li class="info">Asked by <em><?= $value->faq_name_q; ?></em></li>
					<li class="info">Answered by <em><?= $value->faq_name_a; ?></em></li>
					<li class="answer"><?= $value->faq_answer; ?></li>
				</ul>
			</li>
	<?endforeach; ?>
</ul>
